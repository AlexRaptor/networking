//
//  AlamofireNetworkRequest.swift
//  Networking
//
//  Created by Александр Селиванов on 11/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireNetworkRequest {
    
    static var onProgress: ((Double) -> ())?
    static var complete: ((String) -> ())?
    
    static func sendRequest(url: String, completion: @escaping (_ courses: [Course]) -> Void) {
        
        guard let url = URL(string: url) else { return }
        
        request(url).validate().responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                let courses = Course.getArray(from: value)!
                completion(courses)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    /// geting json-string with Alamafire-responseData
    static func responseData(url: String) {
        
        guard let url = URL(string: url) else { return }
        
        request(url).responseData { (responseData) in
            
            switch responseData.result {
                
            case .success(let data):
                guard let string = String(data: data, encoding: .utf8) else { return }
                print(string)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func responseString(url: String) {
        
        guard let url = URL(string: url) else { return }
        
        request(url).responseString { (responseString) in
            
            switch responseString.result {
                
            case .success(let data):
                print(data)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func response(url: String) {
        
        guard let url = URL(string: url) else { return }
        
        request(url).response { (response) in
            
            guard
                let data = response.data,
                let string = String(data: data, encoding: .utf8)
                else { return }
            print(string)
        }
    }
    
    static func fetchImageWithAlamofire(url: String, completion: @escaping (_ image: UIImage) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        request(url).responseData { (responseData) in
            
            switch responseData.result {
                
            case .success(let data):
                guard let image = UIImage(data: data) else { return }
                completion(image)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func downloadImageWithProgress(url: String, completion: @escaping (_ image: UIImage) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        request(url).validate().downloadProgress { (progress) in
            
            print("totalUnitCount: \(progress.totalUnitCount)")
            print("completedUnitCount: \(progress.completedUnitCount)")
            print("fractionCompleted: \(progress.fractionCompleted)")
            print("localizedDescription: \(String(describing: progress.localizedDescription))")
            print("localizedAdditionalDescription: \(String(describing: progress.localizedAdditionalDescription))")
            print("--------------------------------------------------------------------------")
            
            self.onProgress?(progress.fractionCompleted)
            self.complete?(progress.localizedDescription)
            
            }.response { (response) in
                
                guard let data = response.data, let image = UIImage(data: data) else { return }
                
                DispatchQueue.main.async {
                    completion(image)
                }
        }
    }
    
    static func postRequest(url: String, completion: @escaping (_ courses: [Course]) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        let userData: [String: Any] = [ "name":"Our first iOS apps",
                                        "link":"https://swiftbook.ru/contents/our-first-applications/",
                                        "imageUrl":"https://swiftbook.ru/wp-content/uploads/2018/03/2-courselogo.jpg",
                                        "number_of_lessons":20,
                                        "number_of_tests":10 ] as [String : Any]
        
        request(url, method: .post, parameters: userData).responseJSON { (responseJSON) in
            
            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            switch responseJSON.result {
            case .success(let value):
                print(value)
                
                guard
                    let jsonObject = value as? [String: Any],
                    let course = Course(json: jsonObject)
                    else { return }
                
                var courses = [Course]()
                courses.append(course)
                
                completion(courses)
                
            case .failure(let error):
                print(error)
            }            
        }
    }
    
    static func putRequest(url: String, completion: @escaping (_ courses: [Course]) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        let userData: [String: Any] = [ "name":"Our first iOS apps PUT",
                                        "link":"https://swiftbook.ru/contents/our-first-applications/",
                                        "imageUrl":"https://swiftbook.ru/wp-content/uploads/2018/03/2-courselogo.jpg",
                                        "number_of_lessons":20,
                                        "number_of_tests":10 ] as [String : Any]
        
        request(url, method: .put, parameters: userData).responseJSON { (responseJSON) in
            
            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            switch responseJSON.result {
            case .success(let value):
                print(value)
                
                guard
                    let jsonObject = value as? [String: Any],
                    let course = Course(json: jsonObject)
                    else { return }
                
                var courses = [Course]()
                courses.append(course)
                
                completion(courses)
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func uploadImage(url: String) {
        
        guard let url = URL(string: url) else { return }
        
        let httpHeaders = ["Authorization": "Client-ID 1176a39bf53a8ee"]
        
        let image = UIImage.init(named: "tileBg")!
        let imageData = image.pngData()!
        
        upload(multipartFormData: { (multipartFromData) in
            multipartFromData.append(imageData, withName: "image")
        }, to: url,
           headers: httpHeaders) { (encodingCompletionResult) in
            
            switch encodingCompletionResult {
            case .success(request: let uploadRequest,
                          streamingFromDisk: let streamingFromDisk,
                          streamFileURL: let streamFileURL):
                print(request)
                print(streamingFromDisk)
                print(streamFileURL ?? "streamFileURL is NIL")
                
                uploadRequest.validate().responseJSON(completionHandler: { (response) in
                    
                    switch response.result {
                    case .success(let value):
                        print(value)
                    case .failure(let error):
                        print(error)
                    }                    
                })
            case .failure(let error):
                print(error)
            }
        }
    }
}
