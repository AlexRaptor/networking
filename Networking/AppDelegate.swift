//
//  AppDelegate.swift
//  Networking
//
//  Created by Александр Селиванов on 05/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Firebase
import GoogleSignIn

let primaryColor = UIColor.init(hexValue: "#2193B0", alpha: 1) ?? .blue
let secondaryColor = UIColor.init(hexValue: "#6DD5ED", alpha: 1) ?? .cyan

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var bgSessionCompletionHandler: (() -> ())?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        FBSDKApplicationDelegate.sharedInstance()?.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        FirebaseApp.configure()
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        /* for Facebook
        let appId = FBSDKSettings.appID()
        
        if url .scheme != nil && url.scheme!.hasPrefix("fb\(appId ?? "")") && url.host == "authorize" {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }
        
        return false
        */
        
        // For google
        return GIDSignIn.sharedInstance()
            .handle(url,
                    sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                    annotation: [:])
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        bgSessionCompletionHandler = completionHandler
    }
}
