//
//  CollectionViewCell.swift
//  Networking
//
//  Created by Александр Селиванов on 10/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
}
