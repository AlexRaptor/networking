//
//  Course.swift
//  Networking
//
//  Created by Александр Селиванов on 05/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation

/* this is for URLSession and JSONDecoder
struct Course: Decodable {
    let id: Int?
    let name: String?
    let link: String?
    let imageUrl: String?
    let numberOfLessons: Int?
    let numberOfTests: Int?
}
*/

/* this is for Alamofire,
   but it works with URLSession and JSONDecoder too */
struct Course: Decodable {
    let id: Int?
    let name: String?
    let link: String?
    let imageUrl: String?
    let numberOfLessons: Int?
    let numberOfTests: Int?
    
    init?(json: [String: Any]) {
        self.id = json["id"] as? Int
        self.name = json["name"] as? String
        self.link = json["link"] as? String
        self.imageUrl = json["imageUrl"] as? String
        self.numberOfLessons = json["numberOfLessons"] as? Int
        self.numberOfTests = json["numberOfTests"] as? Int
    }
    
    static func getArray(from jsonArray: Any) -> [Course]? {
        guard let jsonArray = jsonArray as? Array<[String: Any]> else { return nil }
        return jsonArray.compactMap { Course(json: $0) }
    }
}

