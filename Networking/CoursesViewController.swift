//
//  TableViewController.swift
//  Networking
//
//  Created by Александр Селиванов on 05/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit

class CoursesViewController: UITableViewController {
    private let url = "https://swiftbook.ru/wp-content/uploads/api/api_courses"
    private let postURL = "https://jsonplaceholder.typicode.com/posts"
    private let putURL = "https://jsonplaceholder.typicode.com/posts/1"

    private var courses = [Course]()
    private var courseURL: String?
    private var courseName: String?

    // MARK: - UITableViewDataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell

        configureTableCell(cell: cell, course: courses[indexPath.row])

        return cell
    }

    private func configureTableCell(cell: TableViewCell, course: Course) {
        cell.courseNameLabel.text = course.name

        if let numberOfLessons = course.numberOfLessons {
            cell.numberOfLessonsLabel.text = "Number of Lessons: \(numberOfLessons)"
        }

        if let numberOfTests = course.numberOfTests {
            cell.numberOfTestsLabel.text = "Number of Tests: \(numberOfTests)"
        }

        DispatchQueue.global().async {
            guard let url = URL(string: course.imageUrl!) else { return }
            guard let imageData = try? Data(contentsOf: url) else { return }

            DispatchQueue.main.async {
                cell.courseImage.image = UIImage(data: imageData)
            }
        }
    }

    // MARK: - UITableViewDelegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let course = courses[indexPath.row]

        courseURL = course.link
        courseName = course.name

        performSegue(withIdentifier: "Description", sender: self)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Description" {
            let destinationVC = segue.destination as! DescriptionViewController

            destinationVC.courseName = courseName
            destinationVC.courseURL = courseURL
        }
    }

    // MARK: - Data

    func fetchData() {
        NetworkManager.fetchCoursesData(url: url) { courses in
            self.courses = courses

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func fetchDataWithAlamofire() {
        AlamofireNetworkRequest.sendRequest(url: url) { courses in
            self.courses = courses
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func postRequestWithAlamofire() {
        AlamofireNetworkRequest.postRequest(url: postURL) { (courses) in
            self.courses = courses
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func putRequestWithAlamofire() {
        AlamofireNetworkRequest.putRequest(url: putURL) { (courses) in
            self.courses = courses
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}
