//
//  DescriptionViewController.swift
//  Networking
//
//  Created by Александр Селиванов on 06/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit
import WebKit

class DescriptionViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var courseURL: String?
    var courseName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = courseName ?? "Course"
        
        guard let courseURL = self.courseURL, let url = URL(string: courseURL) else { return }
        webView.load(URLRequest.init(url: url))
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
