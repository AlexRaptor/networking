//
//  Extensions.swift
//  Networking
//
//  Created by Александр Селиванов on 19/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit

extension UIView {
    
    func addVerticalGradiaentLayer(colors: [UIColor], locations: [NSNumber]? = nil) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.locations = locations
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    enum LinePosition {
        case top
        case bottom
    }
    
    func addLineToView(position : LinePosition, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .top:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .bottom:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}


extension UIColor {

    convenience init?(hexValue: String, alpha: CGFloat) {
        
        if hexValue.hasPrefix("#") && hexValue.count == 7 {
            
            let scaner = Scanner(string: hexValue)
            scaner.scanLocation = 1
            
            var hexInt32: UInt32 = 0
            
            if scaner.scanHexInt32(&hexInt32) {
                
                let red = CGFloat((hexInt32 & 0xFF0000) >> 16) / 255
                let green = CGFloat((hexInt32 & 0x00FF00) >> 8) / 255
                let blue = CGFloat(hexInt32 & 0x0000FF) / 255
                
                self.init(red: red, green: green, blue: blue, alpha: alpha)
                return
            }
        }
        
        return nil
    }
}
