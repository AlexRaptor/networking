//
//  ImageProperties.swift
//  Networking
//
//  Created by Александр Селиванов on 10/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit

struct ImageProperties {
    let key: String
    let data: Data

    init?(withImage image: UIImage, forKey key: String) {
        self.key = key
        guard let data = image.pngData() else { return nil }
        self.data = data
    }
}
