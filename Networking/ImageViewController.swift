//
//  ImageViewController.swift
//  Networking
//
//  Created by Александр Селиванов on 05/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit
import Alamofire

class ImageViewController: UIViewController {

    private let url = "https://applelives.com/wp-content/uploads/2016/03/iPhone-SE-11.jpeg"
    private let largeImageURL = "https://i.imgur.com/3416rvI.jpg"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var completeLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        
//        completeLabel.text = ""
        completeLabel.isHidden = true
        progressBar.isHidden = true
    }
    
    // MARK: - Data
    
    func fetchImage() {
        NetworkManager.downloadImage(url: url) { (image) in
            self.activityIndicator.stopAnimating()
            self.imageView.image = image
        }
    }
    
    func fetchImageWithAlamofire() {
        
        AlamofireNetworkRequest.fetchImageWithAlamofire(url: url) { (image) in
            self.activityIndicator.stopAnimating()
            self.imageView.image = image
        }
    }
    
    func downloadImageWithProgress() {
        
        AlamofireNetworkRequest.onProgress = { (progress) in
            self.progressBar.isHidden = false
            self.progressBar.progress = Float(progress)
        }
        
        AlamofireNetworkRequest.complete = { (string) in
            self.completeLabel.isHidden = false
            self.completeLabel.text = string
        }
        
        AlamofireNetworkRequest.downloadImageWithProgress(url: largeImageURL) { (image) in
            self.activityIndicator.stopAnimating()
            self.progressBar.isHidden = true
            self.completeLabel.isHidden = true
            self.imageView.image = image
        }
    }
}
