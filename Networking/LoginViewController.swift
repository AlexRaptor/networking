//
//  LoginViewController.swift
//  Networking
//
//  Created by Александр Селиванов on 13/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn

class LoginViewController: UIViewController {

    var userProfile: UserProfile?
    
    lazy var fbLoginButton: UIButton = {
        let loginButton = FBSDKLoginButton()
        loginButton.center = CGPoint(x: self.view.center.x, y: self.view.frame.height - loginButton.frame.height / 2 - 20)
        loginButton.delegate = self
        return loginButton
    }()
    
    lazy var customFBLoginButton: UIButton = {
       let loginButton = UIButton()
        loginButton.backgroundColor = UIColor(hexValue: "#3B5999", alpha: 1)
        loginButton.setTitle("Login with Facebook", for: .normal)
        loginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.frame = CGRect(x: 32, y: fbLoginButton.frame.origin.y - 20 - 50, width: self.view.frame.width - 64, height: 50)
        loginButton.layer.cornerRadius = 4
        loginButton.addTarget(self, action: #selector(handleCustomFBLogin), for: .touchUpInside)
        return loginButton
    }()
    
    lazy var googleLoginButton: GIDSignInButton = {
       let loginButton = GIDSignInButton()
        loginButton.frame = CGRect(x: 32, y: customFBLoginButton.frame.origin.y - 20 - 50, width: self.view.frame.width - 64, height: 50)
        return loginButton
    }()
    
    lazy var customGoogleLoginButton: UIButton = {
        let loginButton = UIButton()
        loginButton.backgroundColor = .white
        loginButton.setTitle("Login with Google", for: .normal)
        loginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        loginButton.setTitleColor(.gray, for: .normal)
        loginButton.frame = CGRect(x: 32, y: googleLoginButton.frame.origin.y - 20 - 50, width: self.view.frame.width - 64, height: 50)
        loginButton.layer.cornerRadius = 4
        loginButton.addTarget(self, action: #selector(handleCustomGoogleLogin), for: .touchUpInside)
        return loginButton
    }()
    
    lazy var signInWithEmailButton: UIButton = {
        let loginButton = UIButton()
        loginButton.setTitle("Sign In with Email", for: .normal)
        loginButton.frame = CGRect(x: 32, y: customGoogleLoginButton.frame.origin.y - 20 - 50, width: self.view.frame.width - 64, height: 50)
        loginButton.addTarget(self, action: #selector(openSignInVC), for: .touchUpInside)
        return loginButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addVerticalGradiaentLayer(colors: [primaryColor, secondaryColor])
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        setupViews()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    private func setupViews() {
        
        self.view.addSubview(fbLoginButton)
        self.view.addSubview(customFBLoginButton)
        self.view.addSubview(googleLoginButton)
        self.view.addSubview(customGoogleLoginButton)
        self.view.addSubview(signInWithEmailButton)
    }
    
    @objc private func openSignInVC() {
        performSegue(withIdentifier: "SignIn", sender: self)
    }
}

// MARK: - Facebook SDK

extension LoginViewController: FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        if error != nil {
            print(error)
            return
        }
        
        guard FBSDKAccessToken.currentAccessTokenIsActive() else { return }

        self.signIntoFirebase()
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logged out")
    }
    
    func openMainViewController() {
        dismiss(animated: true)
    }
    
    @objc private func handleCustomFBLogin() {
    
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            guard let result = result else { return }
            
            if result.isCancelled {
                return
            } else {
                
                self.signIntoFirebase()
            }
        }
    }
    
    private func signIntoFirebase() {
        
        let accessToken = FBSDKAccessToken.current()
        
        guard let accessTokenString = accessToken?.tokenString else { return }
        
        let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
        
        Auth.auth().signInAndRetrieveData(with: credentials) { (user, error) in
            
            if let error = error {
                print("Something went wrong with our Facebook user: ", error)
                return
            }
            
            print("Successfully logged in with our Facebook user: ", user!)
            
            self.fetchFacebookFields()
        }
    }
    
    private func fetchFacebookFields() {
        
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"])?.start(completionHandler: { (_, result, error) in
            
            if let error = error {
                print("Failed fetching user data from FB with error: ", error)
                return
            }
            
            guard let userData = result as? [String: Any] else { return }
            self.userProfile = UserProfile(data: userData)
            
            print(userData)
            print(self.userProfile?.name ?? "nil")
            
            self.saveIntoFirebase()
        })
    }
    
    private func saveIntoFirebase() {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let userData = ["name": userProfile?.name, "email": userProfile?.email]
        let values = [uid: userData]
        
        Database.database().reference().child("users").updateChildValues(values) { (error, _) in
            
            if let error = error {
                print(error)
                return
            }
            
            print("Successfully saved user data into Firebase database")
            self.openMainViewController()
        }
    }
}

// MARK: - Google SDK

extension LoginViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("Failed to logged into Google: ", error)
            return
        }
        
        print("Successfully logged into Google")
        
        if let userName = user.profile.name, let userEmail = user.profile.email {
            let userData = ["name": userName, "email": userEmail]
            userProfile = UserProfile(data: userData)
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (user, error) in
            
            if let error = error {
                print("Something went wrong with our Google user: ", error)
                return
            }
            
            print("Successfully logged into Firebase with Google user: ", user!)
            
            self.saveIntoFirebase()
        }
    }
    
    @objc private func handleCustomGoogleLogin() {
        GIDSignIn.sharedInstance()?.signIn()
    }
}
