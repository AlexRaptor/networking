//
//  MainViewController.swift
//  Networking
//
//  Created by Александр Селиванов on 10/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit
import UserNotifications
import FBSDKLoginKit
import FirebaseAuth

enum Actions: String, CaseIterable {
    case downloadImage = "Download Image"
    case get = "GET"
    case post = "POST"
    case ourCourses = "Our Courses"
    case uploadImage = "Upload Image"
    case downloadFile = "Download File"
    case ourCoursesAlamofire = "Our Courses (Alamofire)"
    case responseDataAlamofire = "Response Data (Alamofire)"
    case responseStringAlamofire = "Response String (Alamofire)"
    case responseAlamofire = "Response (Alamofire)"
    case downloadImageWithProgress = "Download Image (Progress AF)"
    case postAlamofire = "POST (Alamofire)"
    case putAlamofire = "PUT (Alamofire)"
    case uploadImageAlamofire =  "Upload Image (Alamofire)"
    
}

private let reuseIdentifier = "Cell"
private let url = "https://jsonplaceholder.typicode.com/posts"
private let uploadImageURL = "https://api.imgur.com/3/image"
private let swiftbookAPIURL = "https://swiftbook.ru/wp-content/uploads/api/api_courses"

class MainViewController: UICollectionViewController {
    let actions = Actions.allCases
    
    private let dataProvider = DataProvider()
    private var alert: UIAlertController!
    private var filePath: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerForNotifications()
        
        dataProvider.fileLocation = { (location) in
            print("Did finish downloading: \(location.absoluteString)")
            self.filePath = location.absoluteString
            
            self.alert.dismiss(animated: false, completion: nil)
            self.postNotification()
        }
        
        checkLoggedIn()
    }
    
    private func showAlert() {
        alert = UIAlertController(title: "Downloading ...", message: "0%", preferredStyle: .alert)
        
        let height = NSLayoutConstraint(item: alert.view,
                                        attribute: .height,
                                        relatedBy: .equal,
                                        toItem: nil,
                                        attribute: .notAnAttribute,
                                        multiplier: 0,
                                        constant: 170)
        
        alert.view.addConstraint(height)
        
        let cancelAction = UIAlertAction(title: "cancel", style: .destructive) { _ in
            self.dataProvider.stopDownload()
        }
        
        alert.addAction(cancelAction)
        
        present(alert, animated: true) {
            let size = CGSize(width: 40, height: 40)
            let point = CGPoint(x: self.alert.view.frame.width / 2 - size.width / 2, y: self.alert.view.frame.height / 2 - size.height / 2)
            
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(origin: point, size: size))
            
            activityIndicator.color = .gray
            activityIndicator.startAnimating()
            
            let progressView = UIProgressView(frame: CGRect(x: 0, y: self.alert.view.frame.height - 44, width: self.alert.view.frame.width, height: 2))
            
            self.dataProvider.onProgress = { (progress) in
                progressView.progress = Float(progress)
                self.alert.message = String(Int(progress * 100)) + "%"
            }
            
            self.alert.view.addSubview(activityIndicator)
            self.alert.view.addSubview(progressView)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let coursesVC = segue.destination as? CoursesViewController
        let imageVC = segue.destination as? ImageViewController
        
        switch segue.identifier {
        case "OurCourses":
            coursesVC?.fetchData()
        case "OurCoursesWithAlamofire":
            coursesVC?.fetchDataWithAlamofire()
        case "ShowImage":
            imageVC?.fetchImage()
        case "ResponseDataAlamofire":
            imageVC?.fetchImageWithAlamofire()
        case "ShowImageWithProgress":
            imageVC?.downloadImageWithProgress()
        case "PostRequestWithAlamofire":
            coursesVC?.postRequestWithAlamofire()
        case "PutRequestWithAlamofire":
            coursesVC?.putRequestWithAlamofire()
        default:
            break
        }
    }
    
    // MARK: - UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actions.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        cell.label.text = actions[indexPath.row].rawValue
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let action = actions[indexPath.row]
        
        switch action {
        case .downloadImage:
            performSegue(withIdentifier: "ShowImage", sender: self)
        case .get:
            NetworkManager.getRequest(url: url)
        case .post:
            NetworkManager.postRequest(url: url)
        case .ourCourses:
            performSegue(withIdentifier: "OurCourses", sender: self)
        case .uploadImage:
            NetworkManager.uploadImage(url: uploadImageURL)
        case .downloadFile:
            dataProvider.startDownloading()
            showAlert()
        case .ourCoursesAlamofire:
            performSegue(withIdentifier: "OurCoursesWithAlamofire", sender: self)
        case .responseDataAlamofire:
            performSegue(withIdentifier: "ResponseDataAlamofire", sender: self)
            AlamofireNetworkRequest.responseData(url: swiftbookAPIURL) // example of getting json with Alamafire-responseData
        case .responseStringAlamofire:
            AlamofireNetworkRequest.responseString(url: swiftbookAPIURL)
        case .responseAlamofire:
            AlamofireNetworkRequest.response(url: swiftbookAPIURL)
        case .downloadImageWithProgress:
            performSegue(withIdentifier: "ShowImageWithProgress", sender: self)
        case .postAlamofire:
            performSegue(withIdentifier: "PostRequestWithAlamofire", sender: self)
        case .putAlamofire:
            performSegue(withIdentifier: "PutRequestWithAlamofire", sender: self)
        case .uploadImageAlamofire:
            AlamofireNetworkRequest.uploadImage(url: uploadImageURL)
        }
    }
}

// MARK: - Notifications

extension MainViewController {
    
    private func registerForNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (_, _) in }
    }
    
    func postNotification() {
        
        let content = UNMutableNotificationContent()
        
        content.title = "Download complete!"
        content.body = "Your background transfer has completed. File Path: \(filePath!)"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: "TransferComplete", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}

// MARK: - Facebook SDK

extension MainViewController {
    
    private func checkLoggedIn() {
        
        if Auth.auth().currentUser == nil {
        
            DispatchQueue.main.async {
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let loginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                
                self.present(loginViewController, animated: true)
                return
            }
        }
    }
}
