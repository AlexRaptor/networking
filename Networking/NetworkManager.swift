//
//  NetworkManager.swift
//  Networking
//
//  Created by Александр Селиванов on 10/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit

class NetworkManager {
    static func getRequest(url: String) {
        guard let url = URL(string: url) else { return }

        let session = URLSession.shared

        session.dataTask(with: url) { data, response, _ in
            guard let response = response else { return }
            print(response)
            print("\n====================\n")

            guard let data = data else { return }
            print(data)
            print("\n====================\n")

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {
                print(error)
            }
        }.resume()
    }

    static func postRequest(url: String) {
        guard let url = URL(string: url) else { return }

        var request = URLRequest(url: url)

        let userData = ["Course": "Networking", "Lesson": "GET and POST Requests"]
        guard let httpBody = try? JSONSerialization.data(withJSONObject: userData, options: []) else { return }

        request.httpMethod = "POST"
        request.httpBody = httpBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared

        session.dataTask(with: request) { data, response, _ in
            guard let response = response, let data = data else { return }

            print(response)
            print("\n====================\n")

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {
                print(error)
            }
        }.resume()
    }

    static func downloadImage(url: String, completion: @escaping (_ image: UIImage) -> Void) {
        guard let url = URL(string: url) else { return }

        let session = URLSession.shared

        session.dataTask(with: url) { data, _, _ in
            if let data = data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    completion(image)
                }
            }
        }.resume()
    }

    static func fetchCoursesData(url: String, completion: @escaping (_ courses: [Course]) -> Void) {
        guard let url = URL(string: url) else { return }

        URLSession.shared.dataTask(with: url) { data, _, _ in

            guard let data = data else { return }

            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let courses = try decoder.decode([Course].self, from: data)

                completion(courses)
            } catch {
                print("Error serialization JSON", error)
            }

        }.resume()
    }
    
    static func uploadImage(url: String) {
        let image = UIImage.init(named: "tileBg")!
        
        guard let imageProperties = ImageProperties(withImage: image, forKey: "image") else { return }
        guard let url = URL(string: url) else { return }

        let httpHeaders = ["Authorization": "Client-ID 1176a39bf53a8ee"]
        var request = URLRequest.init(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = imageProperties.data
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
                print("\n====================\n")
            }
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
}
