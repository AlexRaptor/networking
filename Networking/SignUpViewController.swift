//
//  SignUpViewController.swift
//  Networking
//
//  Created by Александр Селиванов on 21/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignUpViewController: UIViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    var activityIndicator: UIActivityIndicatorView!
    
    lazy var continueButton: UIButton = {
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
        button.center = CGPoint(x: view.center.x, y: view.frame.height - 100)
        button.setTitle("Continue", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(secondaryColor, for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 4
        button.alpha = 0.5
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addVerticalGradiaentLayer(colors: [primaryColor, secondaryColor])
        view.addSubview(continueButton)
        
        userNameTextField.addLineToView(position: .bottom, color: .white, width: 1)
        emailTextField.addLineToView(position: .bottom, color: .white, width: 1)
        passwordTextField.addLineToView(position: .bottom, color: .white, width: 1)
        confirmPasswordTextField.addLineToView(position: .bottom, color: .white, width: 1)
        
        userNameTextField.addTarget(self, action: #selector(textFieldsChanged), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(textFieldsChanged), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldsChanged), for: .editingChanged)
        confirmPasswordTextField.addTarget(self, action: #selector(textFieldsChanged), for: .editingChanged)
        
        activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = secondaryColor
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.center = CGPoint(x: continueButton.frame.width / 2, y: continueButton.frame.height / 2)
        continueButton.addSubview(activityIndicator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        continueButton.setTitle("Continue", for: .normal)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    private func setCountinueButton(enabled: Bool) {
        
        if enabled {
            continueButton.alpha = 1
        } else {
            continueButton.alpha = 0.5
        }
        
        continueButton.isEnabled = enabled
    }
    
    @objc private func textFieldsChanged() {
        
        guard
            let userName = userNameTextField.text,
            let email = emailTextField.text,
            let password = passwordTextField.text,
            let confirmedPassword = confirmPasswordTextField.text
            else { return }
        
        let formFilled = !userName.isEmpty && !email.isEmpty && !password.isEmpty && confirmedPassword == password
        
        setCountinueButton(enabled: formFilled)
    }
    
    @objc private func keyboardWillAppear(notification: NSNotification) {
        
        let userInfo = notification.userInfo!
        let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        continueButton.center = CGPoint(x: continueButton.center.x, y: view.frame.height - keyboardFrame.height - 8 - continueButton.frame.height / 2)
        
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func handleSignUp() {
        setCountinueButton(enabled: false)
        continueButton.setTitle("", for: .normal)
        activityIndicator.startAnimating()
        
        guard
            let userName = userNameTextField.text,
            let email = emailTextField.text,
            let password = passwordTextField.text
            else { return }
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            
            if let error = error {
                print(error.localizedDescription)
                self.activityIndicator.stopAnimating()
                self.continueButton.setTitle("Continue", for: .normal)
                self.setCountinueButton(enabled: true)
                return
            }
            
            print("Successfully logged into Firebase with User Email")
            
            if let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() {
                
                changeRequest.displayName = userName
                changeRequest.commitChanges(completion: { (error) in
                    
                    if let error = error {
                        print(error.localizedDescription)
                        self.activityIndicator.stopAnimating()
                        self.continueButton.setTitle("Continue", for: .normal)
                        self.setCountinueButton(enabled: true)
                        return
                    }
                    
                    print("User display name changed")
                    
                    self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                })
                
            }
        }
    }
}
