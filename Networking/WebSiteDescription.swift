//
//  WebSiteDescription.swift
//  Networking
//
//  Created by Александр Селиванов on 05/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation

struct WebSiteDescription: Decodable {
    let websiteDescription: String?
    let websiteName: String?
    let courses: [Course]?
}
